# CCOS - Site institucional

Esse repositório contém o [site institucional](https://ccos.misterio.me) do CCOS.

## Sobre

O site é gerado estaticamente utilizando o SSG [Jekyll](https://jekyllrb.com). Usamos como base a sheet css [sakura.css](https://github.com/oxalorg/sakura).

### Estrutura do repositório:

#### Conteúdo (predominantemente markdown)

- `_posts`: Posts da aba de notícias, escritos em markdown. Cada categoria tem uma pasta dentro daqui.
- `_pages`:
  - `index.html`: Página inicial do site
  - `sobre.md`: Página sobre
  - `noticias.md`, `eventos.md`: Páginas listando cada categoria de post
  - `feed.xml`: Gerador de RSS feed
- `_data`: Listas e dados que não são páginas, como a lista de categorias e projetos em destaque

#### Layout e estilo

- `_includes`: Componentes reutilizáveis, como info do membro da equipe, listagem de post, etc.
  - `icones`: Ícones svg
- `_layouts`: Layouts do site
  - `default.html`: Layout principal (usado em todas as páginas)
  - `post.html`: Layout para páginas de post (herda do `default`)
- `_sass`: Parciais de estilos em [sass](https://sass-lang.com)
- `assets`: Scripts, estilos, etc.
  - `style.scss`: Estilo principal da página, também sass
  - `favicon.svg`: Ícone do site

#### Configurações, CI, etc

- `_config.yml`: Configurações do Jekyll
- `.gitlab-ci.yml`: Manifesto para CI/CD do gitlab
- `flake.nix` e `flake.lock`: Manifesto para buildar com [nix](https://nixos.org)

## Como contribuir

### Interface do GitLab

Caso você não saiba como usar git, você pode simplesmente clicar em [Web IDE](https://gitlab.com/-/ide/project/misterio77/ccos-prototipo/edit/main/-/) para abrir o repositório na IDE online do Gitlab. De lá, basta editar o que deseja, e clicar em commit (ou abrir um Merge Request).

### O que editar?

Use a listagem de pastas e arquivos acima como referência. Mas, em suma:

- **Post novo**: Vá para a pasta `_posts`, e então para a categoria desejada, e crie um novo arquivo `.md` (ou `.html`) com data e título (exemplo: `2021-10-21-ola-mundo.md`)

- **Editar uma página/post**: Encontre a página (pasta `_pages`) ou post (pasta `_posts`)

- **Editar algo no layout**: Esses ficam em `_layouts`

- **Editar algo no estilo**: Faça alterações apenas em `assets/style.scss`

### Como executar localmente?

Precisa fazer uma edição mais complexa que editar um markdown?

Siga os passos de instalação no [site do jekyll](https://jekyllrb.com/), clone o repositório, entre no diretório, e use o comando `jekyll serve`.
