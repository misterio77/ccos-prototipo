---
title: CCOS divulga projeto enviado à PUB
author: Deandreson Alves
---

O Centro de Competência Open Source enviou projeto para a seleção de bolsista
por meio do Programa Unificado de Bolsa da Universidade de São Paulo. Destinado
aos estudantes da universidade, o projeto visa selecionar um bolsista para auxiliar
o Centro na divulgação de artefatos de tecnologias Open Source.

Para participar tem que ser aluno, estar regularmente matriculado e possuir
inscrição na PAPFE/2021.

[Leia o projeto enviado à PUB.](http://ccos.icmc.usp.br:8081/wp-content/uploads/2021/10/Nakagawa2021_Website_Divulgacao_CCOS.docx-3.pdf)
